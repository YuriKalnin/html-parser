<?php

namespace App\Services\HtmlParser\CountHtmlTags;

class CountHtmlTags
{
    private $html;
    private $allTags;

    public $tags = [];
    public $status = 1;

    public function handle($html) : self {
        $this
            ->setHtml($html)
            ->findTags()
            ->countTags();

        return $this;
    }

    private function setHtml($html) : self {
        $this->html = $html;
        return $this;
    }

    private function findTags() : self {
        preg_match_all('/<(\w+)>|<(\w+)\s.+?>/', $this->html, $matches);
        $this->allTags = array_filter(array_merge([], $matches[1], $matches[2]));
        return $this;
    }

    private function countTags() : self {
        foreach($this->allTags as $tag) {
            $count = $this->tags[$tag] ?? 0;
            $this->tags[$tag] = $count+1;
        }
        return $this;
    }
}
