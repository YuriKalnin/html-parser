<?php

namespace App\Services\HtmlParser\HtmlLoader;

use Illuminate\Support\Facades\Http;

class HtmlLoader
{
    public $http;

    public function handle($url) : string {
        $this->http = Http::get($url);
        return $this->html();
    }

    public function html() {
        return str_replace(["\r\n", "\r", "\n"], ' ', $this->http->body());
    }
}
