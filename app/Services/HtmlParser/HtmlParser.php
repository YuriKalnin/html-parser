<?php

namespace App\Services\HtmlParser;

use App\Services\HtmlParser\CountHtmlTags\CountHtmlTags;
use App\Services\HtmlParser\HtmlLoader\HtmlLoader;

class HtmlParser
{
    public $htmlLoader;
    public $countHtmlTags;

    public function __construct(
        HtmlLoader $htmlLoader,
        CountHtmlTags $countHtmlTags
    )
    {
        $this->htmlLoader = $htmlLoader;
        $this->countHtmlTags = $countHtmlTags;
    }

    public function countHtmlTags($url) : CountHtmlTags {
        $html = $this->htmlLoader->handle($url);
        return $this->countHtmlTags->handle($html);
    }
}
