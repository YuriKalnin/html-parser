<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Services\HtmlParser\HtmlParser;
use App\Http\Requests\Parser\CounterHtmlTagsRequest;

class ParserController extends Controller
{
    public function counterHtmlTags(CounterHtmlTagsRequest $request, HtmlParser $htmlParser) {

        $url = $request->get('url');

        $countHtmlTags = $htmlParser->countHtmlTags($url);

        return response()->json($countHtmlTags);
    }
}
